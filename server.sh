#!/bin/bash
pwd=$(dirname $0)

case ${HOSTNAME} in
    "hp01")
        ${pwd}/hugo server -D \
        --bind 0.0.0.0 \
        -p 1313
        ;;
    *)
        ${pwd}/hugo server -D \
        --bind 0.0.0.0 \
        --liveReloadPort=443 \
        --appendPort=false \
        --baseURL=https://webdev.g.daniel.doussaud.fr:443/ \
        -p 8082
        ;;
esac


exit
\
   --baseURL=http://webdev.g.daniel.doussaud.fr:443/ \
  -p 8082

#   --appendPort=false \

  #--config config/_default/* \
