---
title: "Introduction"
date: 2020-05-30T14:24:24Z
draft: false
Categories: ["accueil",""]
Tags: ["",""]
img_head: "/stories/vte500/vte route dsertique.jpg"
img_foot: "/stories/vte500/auto_portrait_1024x768.jpg" 
---
Il y a des évènements qui marquent la vie d’un enfant.

Je me souviens de la RN 20, à bord de la voiture parentale. Une longue descente sur Figeac. Soudain, je l’aperçus, rouge, flamboyante, avec deux personnes à son bord . Une Honda 750 Four avec ses bagages accrochés derrière la selle. D’un coup de gaz, elle avalait les voitures les unes après les autres. Libre comme l’air.

Je n’oublierai jamais l’image fugitive de ce deux roues que j’imaginai en partance pour des contrées lointaines.

Je crois que c’est à partir de ce moment que la moto est devenue, dans mon esprit, un véhicule destiné au voyage.

A quatorze ans, mon cyclomoteur m’a permis de passer du rêve à la réalité. Les trois kilomètres qui me séparaient du lycée se multiplièrent et c’est dix kilomètres que je parcourais en utilisant des chemins détournés. J’étais déjà « en voyage » lorsque j’allais au bahut. Mon Peugeot 102 a aussi souvent quitté les rues de ma ville auxquelles il était destiné pour des routes du département et même de la région.

Le pli était pris.

L’arrivée, sept années plus tard, de ma première moto, accéléra le processus. L’appel de la route se fit plus pressant et , très vite, les frontières n’eurent d’autre finalité que d’être traversées, pour aller voir ailleurs, comment la vie s’y déroulait.

Le bonheur étant toujours au rendez-vous, lors de ces voyages, l’envie de repartir fut, jusqu’à présent, la plus forte.

Les récits de voyage et les lectures des cartes ont, bien sûr, nourri ce désir. J’ai découvert l’un d’entre eux, tardivement ; il s’agit de « L’usage du monde » de Nicolas Bouvier qui accomplit un magnifique voyage dans les années 50. J’ai retrouvé, dans sa très belle écriture, toutes ces sensations qui vous pénètrent lorsque vous êtes « sur la route ».

« C’est la contemplation silencieuse des atlas, à plat ventre sur le tapis, entre dix et treize ans, qui donne ainsi l’envie de tout planter là. Songez à des régions comme le Banat, la Caspienne, le Cachemire, aux musiques qui y résonnent, aux regards qu’on y croise, aux idées qui vous y attendent… Lorsque le désir résiste aux premières atteintes du bon sens, on lui cherche des raisons. Et on en trouve qui ne valent rien. La vérité, c’est qu’on ne sait comment nommer ce qui vous pousse. Quelque chose en vous grandit et détache les amarres, jusqu’au jour où, pas trop sûr de soi, on s’en va pour de bon
Un voyage se passe de motifs. Il ne tarde pas à prouver qu’il se suffit à lui-même. On croit qu’on va faire un voyage, mais bientôt, c’est le voyage qui vous fait, ou vous défait ».

« Assez d’argent pour vivre neuf semaines. Ce n’est qu’une petite somme, mais c’est beaucoup de temps. Nous nous refusons tous les luxes, sauf le plus précieux : la lenteur. Le toit ouvert, les gaz à main légèrement tirés, assis sur le dossier du fauteuil et un pied sur le volant, on chemine paisiblement à 20 km/h à travers des paysages qui ont l’avantage de ne pas changer sans avertir ou à travers des nuits de pleine lune qui sont riches en prodiges : lucioles , cantonniers en babouches, modiques bals de villages sous trois peupliers, calmes rivières dont le passeur n’est pas levé et le silence si parfait que le son de votre klaxon vous fait tressaillir. Puis, le jour se lève et le temps ralentit ».

" A mon retour, il s'est trouvé beaucoup de gens qui n'étaient pas partis pour me dire qu'avec un peu de fantaisie et de concentration, ils voyageaient aussi bien sans lever le cul de leur chaise. Je les crois volontiers. Ce sont des forts. Pas moi. J'ai trop besoin de cet apport concret qu'est le déplacement dans l'espace. Heureusement d'ailleurs que le monde s'étend pour les faibles et les supporte, et quand le monde, comme certains soirs sur la route de Macédoine, c'est la lune à main gauche, les flots argentés de la Morava à main droite, et la perspective d'aller chercher derrière l'horizon un village où vivre les trois prochaines semaines, je suis bien aise de ne pouvoir m'en passer".

Quelques uns de ces voyages, peut-être plus intenses que les autres, m’ont donné envie de laisser une trace écrite.

Parfois, j’ai  laissé libre cours à mon imagination en écrivant des textes intitulés « nouvelles ».

« L’imagination en voyage . Tome 1 » et « L’envol. Tome 2 » en sont le résultat. Pour rester dans le sujet, elles racontent l’histoire d’un motard qui décide de découvrir le monde au guidon de son deux roues.

Les autres nouvelles sont des petites tranches de vie d’un motard.

J'ai eu envie de relater la "folle" histoire de Voxan, cette marque française qui est née de la volonté d'un passionné de motos, Jacques Gardette. Malgré bien des difficultés, elle était toujours là en 2009, avec un nouveau modèle commercialisé, la VX 10. Malheureusement,la marque vient de disparaître, après une liquidation judiciaire et une vente aux enchères les 5 et 6 mai 2010.

Enfin, mon site contient des essais de motos dans lesquels je décris mes sensations au guidon de ces montures avec lesquelles j'ai passées quelques jours ou plusieurs années (essais longue durée).

Bonne lecture. .